<?php
require "ConnexionBaseDeDonnees.php";
require "Utilisateur.php";

$pdoStatement = ConnexionBaseDeDonnees::getPdo()->query(/** @lang text */ "SELECT * FROM utilsateur");
foreach ($pdoStatement as $utilisateurFormatTableau) {
    $utilisateur = Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    echo $utilisateur;
}