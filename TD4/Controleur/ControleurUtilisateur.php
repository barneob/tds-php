<?php
require_once('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur
{
    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherListe(): void
    {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();     //appel au modèle pour gérer la BD
        self::afficherVue('utilisateur/liste.php', ['utilisateurs' => $utilisateurs]); //appel à la vue
    }

    public static function afficherDetail(): void
    {
        $Login = $_GET['login'];
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($Login);
        if ($utilisateur == null) {
            self::afficherVue('utilisateur/erreur.php');
        } else {
           self::afficherVue('utilisateur/detail.php', ['utilisateur' => $utilisateur]);
        }
    }
    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('utilisateur/formulaireCreation.php');
    }
    public static function creerDepuisFormulaire(){
        $login = $_GET['login'];
        $nom = $_GET['name'];
        $prenom = $_GET['prenom'];
        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        self::afficherListe();
    }
}
?>