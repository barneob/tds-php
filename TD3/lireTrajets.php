<?php
require "ConnexionBaseDeDonnees.php";
require "Trajet.php";

$pdoStatement = ConnexionBaseDeDonnees::getPdo()->query(/** @lang text */ "SELECT * FROM trajet");
foreach ($pdoStatement as $trajetsFormatTableau) {
    $trajets = Trajet::construireDepuisTableauSQL($trajetsFormatTableau);
    echo $trajets;
    echo "Liste Passagers : ";
    foreach ($trajets->getPassagers() as $passager) {
        echo $passager;
    }
}