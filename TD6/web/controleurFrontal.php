<?php

use App\Covoiturage\Controleur\ControleurUtilisateur as ControleurUtilisateur;

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

$controleur = $_GET['controleur'];
$nomDeClasseControleur = 'App\Covoiturage\Controleur\Controleur' . ucfirst($controleur);
if (class_exists($nomDeClasseControleur)) {
    if (!isset($_GET['action'])) {
        $action = 'afficherListe';
    } else {
        $action = $_GET['action'];
        $toutesActions = get_class_methods(ControleurUtilisateur::class);
        if (!in_array($action, $toutesActions)) {
            $action = 'afficherErreur';
        }
    }
    $nomDeClasseControleur::$action();
} else {
    ControleurUtilisateur::afficherErreur("Erreur Contoleur !!");
}
// Appel de la méthode statique $action de ControleurUtilisateur // Appel de la méthode statique $action de ControleurUtilisateur
?>