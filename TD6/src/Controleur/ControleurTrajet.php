<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Repository\TrajetRepository as TrajetRepository;
use App\Covoiturage\Modele\DataObject\Trajet as Trajet;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet
{
    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer();    //appel au modèle pour gérer la BD
        self::afficherVue('vueGeneral.php', ['controleur' => 'trajet', 'trajets' => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/liste.php"]); //appel à la vue
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGeneral.php', ['controleur' => 'trajet', "titre" => "Formulaire Creation trajet", "cheminCorpsVue" => 'trajet/formulaireCreation.php']);
    }

    public static function creerDepuisFormulaire()
    {

        $trajet1 = self::construireDepuisFormulaire($_GET);
        $TrajetRepository = new TrajetRepository();
        $TrajetRepository->ajouter($trajet1);
        $listTrajets = $TrajetRepository->recuperer();
        self::afficherVue('vueGeneral.php', ["controleur" => 'trajet', 'trajets' => $listTrajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/trajetCree.php"]);
    }
    public static function afficherFormulaireMiseAJour(): void
    {
        $id = $_GET['id'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        if ($trajet == null) {
            self::afficherErreur("Trajet inconnu");
        } else {
            self::afficherVue('vueGeneral.php', ['controleur' => 'trajet', "titre" => "Formulaire Mise à jour trajet", "cheminCorpsVue" => 'trajet/formulaireMiseAJour.php', 'trajet' => $trajet]);
        }
    }
     public static function mettreAJour(): void
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        $trajetRepository = new TrajetRepository();
        $trajetRepository->mettreAJour($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGeneral.php', ['controleur' => 'trajet', 'trajets' => $trajets, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "trajet/trajetMisAJour.php", 'id' => $trajet->getId()]);
    }

    public static function afficherDetail(): void
    {
        $id = $_GET['id'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        if ($trajet == null) {
            self::afficherErreur("Trajet Introuvable");
        } else {
            self::afficherVue('vueGeneral.php', ['controleur' => 'trajet', "titre" => "Detail Trajet", "cheminCorpsVue" => "trajet/detail.php", "trajet" => $trajet]);
        }
    }

    public static function afficherErreur(string $erreur = ""): void
    {
        self::afficherVue('vueGeneral.php', ["titre" => "ERREUR Trajet !!", "cheminCorpsVue" => "trajet/erreur.php", "erreur" => $erreur]);
    }

    public static function supprimer(): void
    {
        $id = $_GET['id'];
        $TrajetRepository = new TrajetRepository();
        $TrajetRepository->supprimer($id);
        $trajets = $TrajetRepository->recuperer();
        self::afficherVue('vueGeneral.php', ['controleur' => 'trajet', 'trajets' => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/trajetSupprimer.php", 'id' => $id]);
    }

    /**
     * @return Trajet
     * @throws \DateMalformedStringException
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $_GET['id'];
        $depart = $_GET['depart'];
        $arrivee = $_GET['arrivee'];
        $Date = $_GET['date'];
        $prix = $_GET['prix'];
        $conducteurLogin = $_GET['conducteurLogin'];
        $nonFumeur = $_GET['nonFumeur'];
        $trajet1 = new Trajet((empty($id)) ? null : (int)$id, $depart, $arrivee, new DateTime($Date), $prix, (new UtilisateurRepository)->recupererParClePrimaire($conducteurLogin), isset($nonFumeur), array());
        return $trajet1;
    }
}

?>