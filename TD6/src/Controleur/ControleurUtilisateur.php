<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository as UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur as Utilisateur;

class ControleurUtilisateur
{
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer();     //appel au modèle pour gérer la BD
        self::afficherVue('vueGeneral.php', ['controleur' => 'utilisateur', 'utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]); //appel à la vue
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherDetail(): void
    {
        $Login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($Login);
        if ($utilisateur == null) {
            self::afficherErreur("Utilisateur inconnu");
        } else {
            self::afficherVue('vueGeneral.php', ['controleur' => 'utilisateur', "titre" => "Détails Utilisateur", "cheminCorpsVue" => 'utilisateur/detail.php', 'utilisateur' => $utilisateur]);
        }
    }

    public static function afficherErreur(string $erreur = ""): void
    {
        self::afficherVue('vueGeneral.php', ["titre" => "ERREUR Utilisateur !!", "cheminCorpsVue" => "utilisateur/erreur.php", "erreur" => $erreur]);
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGeneral.php', ['controleur' => 'utilisateur', "titre" => "Formulaire Creation utilisateur", "cheminCorpsVue" => 'utilisateur/formulaireCreation.php']);
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur == null) {
            self::afficherErreur("Utilisateur inconnu");
        } else {
            self::afficherVue('vueGeneral.php', ['controleur' => 'utilisateur', "titre" => "Formulaire Mise à jour utilisateur", "cheminCorpsVue" => 'utilisateur/formulaireMiseAJour.php', 'utilisateur' => $utilisateur]);
        }
    }

    public static function creerDepuisFormulaire()
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        $UtilisateurRepository = new UtilisateurRepository();
        $UtilisateurRepository->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGeneral.php', ["controleur" => 'utilisateur', 'utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $_GET['login'];
        $nom = $_GET['name'];
        $prenom = $_GET['prenom'];
        $utilisateur = new Utilisateur($login, $nom, $prenom);
        return $utilisateur;
    }

    public static function supprimer(): void
    {
        $login = $_GET['login'];
        $UtilisateurRepository = new UtilisateurRepository();
        $UtilisateurRepository->supprimer($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGeneral.php', ['controleur' => 'utilisateur', 'utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php", 'login' => $login]);
    }

    public static function mettreAJour(): void
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        $UtilisateurRepository = new UtilisateurRepository();
        $UtilisateurRepository->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGeneral.php', ['controleur' => 'utilisateur', 'utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php", 'login' => $login]);
    }
}

?>