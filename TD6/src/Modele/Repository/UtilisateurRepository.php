<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur as Utilisateur;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\Repository\AbstractRepository;

class UtilisateurRepository extends AbstractRepository
{
    // Pour pouvoir convertir un objet en chaîne de caractères
    /*public function __toString(): string
    {
        return "$this->prenom $this->nom de login $this->login";
    }*/

    public function getNomTable(): string
    {
        return 'utilisateur';
    }

    public function getNomClePrimaire(): string
    {
        return 'login';
    }

    protected function construireDepuisTableauSQL(array $objetFormatTableau): Utilisateur
    {
        $utilisateur = new Utilisateur($objetFormatTableau[0], $objetFormatTableau[1], $objetFormatTableau[2]);
        return $utilisateur;
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom"];
    }

    protected function formatTableauSQL(AbstractDataObject $objet): array
    {
        /** @var Utilisateur $objet */
        return array(
            "loginTag" => $objet->getLogin(),
            "nomTag" => $objet->getNom(),
            "prenomTag" => $objet->getPrenom(),
        );
    }
}