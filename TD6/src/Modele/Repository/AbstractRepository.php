<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;

abstract class AbstractRepository
{
    public function mettreAJour(AbstractDataObject $object): void
    {
        $nomTable = $this->getNomTable();
        $nomColonnes = $this->getNomsColonnes();
        $valeurSet = array();
        for ($i = 0; $i < count($nomColonnes); $i++) {
            $valeur = $nomColonnes[$i] . "= :" . $nomColonnes[$i] . "Tag";
            $valeurSet[$i] = $valeur;
        }
        $nomColonnesSet = join(", ", $valeurSet);
        $clePrimaire = $this->getNomClePrimaire();
        $TagClePrimaire = ":" . $clePrimaire . "Tag";

        $sql = "UPDATE " . $nomTable . " SET " . $nomColonnesSet . " WHERE " . $clePrimaire . "=" . $TagClePrimaire;
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = $this->formatTableauSQL($object);

        $pdoStatement->execute($values);
    }

    public
    function ajouter(AbstractDataObject $objet): bool
    {
        $nomtable = $this->getNomTable();
        $nomsColonnes = join(",", $this->getNomsColonnes());
        $tagColonnes = ":" . join("Tag, :", $this->getNomsColonnes()) . "Tag";
        $sql = "INSERT INTO $nomtable ($nomsColonnes) VALUES ($tagColonnes)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = $this->formatTableauSQL($objet);
        $pdoStatement->execute($values);

        return true;
    }

    protected

    abstract function getNomTable(): string;

    /** @return string[] */
    protected abstract function getNomsColonnes(): array;

    public function supprimer($valeurClePrimaire): void
    {

        $sql = "DELETE FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = " . "'" . $valeurClePrimaire . "'";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $pdoStatement->execute();
    }

    protected abstract function getNomClePrimaire(): string;

    public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = " . "'" . $clePrimaire . "'";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute();

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $objetFormatTableau = $pdoStatement->fetch();
        if (!$objetFormatTableau) {
            return null;
        } else {
            return $this::construireDepuisTableauSQL($objetFormatTableau);
        }
    }

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau): AbstractDataObject;

    /**
     * @return AbstractDataObject[]
     */
    public function recuperer(): array
    {
        $nomTable = $this->getNomTable();
        $sql = "SELECT * FROM " . $nomTable;
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        // On exécute la requête
        $pdoStatement->execute();

        while ($tableDonneesFormatTableau = $pdoStatement->fetch()) {
            $TableDonnees[] = $this::construireDepuisTableauSQL($tableDonneesFormatTableau);
        }
        return $TableDonnees;
    }

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;
}

?>