<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet as Trajet;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use DateTime;
use App\Covoiturage\Modele\Repository\AbstractRepository;

class TrajetRepository extends AbstractRepository

{
    protected
    function construireDepuisTableauSQL(array $objetFormatTableau): Trajet
    {
        $trajet = new Trajet(
            $objetFormatTableau["id"],
            $objetFormatTableau["depart"],
            $objetFormatTableau["arrivee"],
            new DateTime($objetFormatTableau["date"]), // À changer
            $objetFormatTableau["prix"],
            (new UtilisateurRepository)->recupererParClePrimaire($objetFormatTableau["conducteurLogin"]), // À changer
            $objetFormatTableau["nonFumeur"],// À changer ?
            array()
        );
        $trajet->setPassagers(self::recupererPassagers($trajet));
        return $trajet;
    }

    /**
     * @return Utilisateur[]
     */
    static
    public function recupererPassagers(Trajet $trajet): array
    {
        $sql = "SELECT passagerLogin FROM passager p JOIN trajet t ON p.trajetId = t.id WHERE p.trajetId = :trajetValues";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "trajetValues" => $trajet->getId(),
        );
        $passagers = array();
        $pdoStatement->execute($values);

        $passagerFormatTableau = $pdoStatement->fetch();
        while ($passagerFormatTableau) {
            $Utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($passagerFormatTableau["passagerLogin"]);
            $passagers[] = $Utilisateur;
            $passagerFormatTableau = $pdoStatement->fetch();
        }
        return $passagers;
    }

    public function getNomTable(): string
    {
        return 'trajet';
    }

    public function getNomClePrimaire(): string
    {
        return 'id';
    }

    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $objet): array
    {
        /** @var Trajet $objet */
        return array(
            "idTag" => $objet->getId(),
            "departTag" => $objet->getDepart(),
            "arriveeTag" => $objet->getArrivee(),
            "dateTag" => $objet->getDate()->format("Y-m-d"),
            "prixTag" => $objet->getPrix(),
            "conducteurLoginTag" => $objet->getConducteur()->getLogin(),
            "nonFumeurTag" =>  intval($objet->isNonFumeur()),
        );
    }


}