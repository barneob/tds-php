<?php

namespace App\Covoiturage\Modele\DataObject;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class Utilisateur extends AbstractDataObject
{
    private string $login;
    private string $nom;
    private string $prenom;

    // un getter

    public function __construct(
        string $login,
        string $nom,
        string $prenom
    )
    {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    // un setter

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    // un constructeur

    public function setLogin(string $Login)
    {
        $this->login = substr($Login, 0, 64);
    }
}

?>
