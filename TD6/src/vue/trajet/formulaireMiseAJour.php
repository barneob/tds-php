<?php

use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\DataObject\Trajet;

/**
 * @var Trajet $Trajet
 */
$trajetRepository = new TrajetRepository();
$Trajet = $trajetRepository->recupererParClePrimaire($_GET['id']);
?>
<body>
<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type='hidden' name='id' id="idTrajet_id" value=<?php echo($Trajet->getId()); ?>>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Depart&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : <?php echo ($Trajet->getDepart());?>" name="depart" id="depart_id"
                   required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivee&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : <?php echo ($Trajet->getArrivee());?>" name="arrivee" id="arrivee_id"
                   required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date&#42;</label>
            <input class="InputAddOn-field" type="date" data-date-format="Y-M-d" placeholder="Ex : 2024-11-05"
                   name="date" id="date_id">
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix&#42;</label>
            <input class="InputAddOn-field" type="number" placeholder="Ex : <?php echo ($Trajet->getPrix());?> " name="prix" id="prix_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteurLogin">conducteurLogin&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : <?php echo ($Trajet->getConducteur()->getLogin());?>" name="conducteurLogin"
                   id="conducteurLogin_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur">nonFumeur&#42;</label>
            <input class="InputAddOn-field" type="checkbox" name="nonFumeur"
                   id="nonFumeur_id"<?php if ($Trajet->isNonFumeur()) {
                echo "checked";
            }; ?> required>
        </p>
        <p>
            <input type="submit" value="Envoyer"/>
            <input type='hidden' name='action' value='mettreAJour'>
            <input type='hidden' name='controleur' value='trajet'>
        </p>
    </fieldset>
</form>
</body>