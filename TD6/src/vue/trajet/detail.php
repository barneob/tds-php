<body>
<?php

use App\Covoiturage\Modele\DataObject\Trajet;

/** @var Trajet $trajet */
$nonFumeur = $trajet->isNonFumeur() ? " non fumeur" : " ";
echo '<p> Le trajet ' . $nonFumeur . "du " . $trajet->getDate()->format("d/m/Y") . " partira de " . htmlspecialchars($trajet->getDepart()) . " pour aller à " . htmlspecialchars($trajet->getArrivee()) . " (conducteur : " . htmlspecialchars($trajet->getConducteur()->getLogin()) . ").</p>"
?>
</body>