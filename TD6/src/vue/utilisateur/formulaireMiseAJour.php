<?php

use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;

/**
 * @var Utilisateur $utilisateur
 */
$utilisateurRepository = new UtilisateurRepository();
$utilisateur = $utilisateurRepository->recupererParClePrimaire($_GET['login']);
?>

<body>
<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">nom&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : <?php echo $utilisateur->getNom() ?>"
                   name="name" id="nom_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">prenom&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : <?php echo $utilisateur->getPrenom() ?>"
                   name="prenom" id="prenom_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text"
                   placeholder="Impossible de changer de login : <?php echo $utilisateur->getLogin() ?>" name="login"
                   id="login_id" value=<?php echo $utilisateur->getLogin()?> readonly required">
        </p>
        <p>
            <input type="submit" value="Envoyer"/>
            <input type='hidden' name='action' value='mettreAJour'>
            <input type='hidden' name='controleur' value='utilisateur'>
        </p>
    </fieldset>
</form>
</body>