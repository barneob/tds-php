<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>

    <body>
        <p>
            <?php
            echo "<h2>Liste des étudiants :<br></h2>";
            $listeEtudiant = [];
             $utilisateur1 =[
                     'nom' => 'Gator',
                 'prenom' => 'Ali',
                 'login' => 'GatorA',
             ];

            $utilisateur2 =[
                'nom' => 'Baba',
                'prenom' => 'Ali',
                'login' => 'BabaA',
            ];

            $utilisateur3 =[
                'nom' => 'Leponge',
                'prenom' => 'Bob',
                'login' => 'LepongeB',
            ];
            $listeEtudiant= [
                'Ult1' => $utilisateur1,
                'Ult2' => $utilisateur2,
                'Ult3' => $utilisateur3,
            ];
            echo "<ul>";
            if (empty($listeEtudiant)){
                echo  "<li>La liste est vide</li>";
            }else{
            foreach ($listeEtudiant as $etudiant) {
                echo "<li>{$etudiant['nom']}{$etudiant['prenom']} de login {$etudiant['login']}</li>\n";
            }
            }
            echo "</ul>";
             ?>
        </p>
    </body>
</html> 