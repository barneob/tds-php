<?php

namespace App\Covoiturage\Modele;

use App\Covoiturage\Modele\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;

class   ModeleUtilisateur
{
    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    // un setter
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    public function setLogin(string $Login)
    {
        $this->login = substr($Login, 0, 64);
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
    )
    {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    /*public function __toString(): string
    {
        return "$this->prenom $this->nom de login $this->login";
    }*/

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): ModeleUtilisateur
    {
        $utilisateur = new ModeleUtilisateur($utilisateurFormatTableau[0], $utilisateurFormatTableau[1], $utilisateurFormatTableau[2]);
        return $utilisateur;
    }

    public static function recupererUtilisateurParLogin(string $login): ?ModeleUtilisateur
    {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if (!$utilisateurFormatTableau) {
            return null;
        } else {
            return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
    }

    public static function recupererUtilisateurs()
    {
        $sql = "SELECT * from utilisateur";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        // On exécute la requête
        $pdoStatement->execute();

        // On récupère les résultats
        $utilisateurs = array();
        while ($utilisateurFormatTableau = $pdoStatement->fetch()) {
            $utilisateurs[] = ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }

    public function ajouter(): void
    {
        $sql = "INSERT INTO utilisateur VALUES (:loginValues,:nomValues,:prenomValues)";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginValues" => $this->getLogin(),
            "nomValues" => $this->getNom(),
            "prenomValues" => $this->getPrenom()
        );

        $pdoStatement->execute($values);
    }
}

?>
