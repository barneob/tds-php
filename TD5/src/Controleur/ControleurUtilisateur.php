<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\ModeleUtilisateur as ModeleUtilisateur;

class ControleurUtilisateur
{
    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherListe(): void
    {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();     //appel au modèle pour gérer la BD
        self::afficherVue('vueGeneral.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]); //appel à la vue
    }

    public static function afficherDetail(): void
    {
        $Login = $_GET['login'];
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($Login);
        if ($utilisateur == null) {
            self::afficherVue('vueGeneral.php', ["titre" => "ERREUR Utilisateur !!", "cheminCorpsVue" => "utilisateur/erreur.php"]);
        } else {
            self::afficherVue('vueGeneral.php', ["titre" => "Détails Utilisateur", "cheminCorpsVue" => 'utilisateur/detail.php', 'utilisateur' => $utilisateur]);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGeneral.php', ["titre" => "Formulaire Creation utilisateur", "cheminCorpsVue" => 'utilisateur/formulaireCreation.php']);
    }

    public static function creerDepuisFormulaire()
    {
        $login = $_GET['login'];
        $nom = $_GET['name'];
        $prenom = $_GET['prenom'];
        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue('vueGeneral.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }
}

?>