<body>
<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">nom&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Baba" name="name" id="nom_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">prenom&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Ali" name="prenom" id="prenom_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : BabaA" name="login" id="login_id" required>
        </p>
        <p>
            <input type="submit" value="Envoyer"/>
            <input type='hidden' name='action' value='creerDepuisFormulaire'>
        </p>
    </fieldset>
</form>
</body>